//
//  NSColor+Additions.m
//  VNModel
//
//  Created by aarthur on 2/13/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import "NSColor+Additions.h"

@implementation NSColor (Additions)

+ (NSColor*)playMakerRed
{
    return [NSColor colorWithRed:165/255.f green:54/255.f blue:63/255.f alpha:1.0];
}

+ (NSColor*)playMakerDarkBlue
{
    return [NSColor colorWithRed:12/255.f green:55/255.f blue:84/255.f alpha:1.0];
}

+ (NSColor*)playMakerBlue
{
    return [NSColor colorWithRed:65/255.f green:122/255.f blue:160/255.f alpha:1.0];
}

+ (NSColor*)playMakerLightBlue
{
    return [NSColor colorWithRed:180/255.f green:211/255.f blue:232/255.f alpha:1.0];
}

@end
