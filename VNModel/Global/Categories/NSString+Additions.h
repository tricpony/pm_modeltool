//
//  NSString+Additions.h
//  VNModel
//
//  Created by aarthur on 2/12/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString*)longestCommonSubsequence:(NSString*)string;
- (NSArray*)lcsDiff:(NSString*)string;
- (NSArray*)rangesOflcsDiff:(NSString*)string;

@end
