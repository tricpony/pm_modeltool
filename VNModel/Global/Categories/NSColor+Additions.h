//
//  NSColor+Additions.h
//  VNModel
//
//  Created by aarthur on 2/13/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (Additions)

+ (NSColor*)playMakerRed;
+ (NSColor*)playMakerDarkBlue;
+ (NSColor*)playMakerBlue;
+ (NSColor*)playMakerLightBlue;

@end
