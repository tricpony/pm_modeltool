//
//  Constants.h
//  VNModel
//
//  Created by aarthur on 2/10/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstantsDefines.h"

#pragma mark - Basic Keys
FOUNDATION_EXPORT NSString * const API_EmailKey;
FOUNDATION_EXPORT NSString * const API_PasswordKey;
FOUNDATION_EXPORT NSString * const API_VersionKey;
FOUNDATION_EXPORT NSString * const API_ModelKey;
FOUNDATION_EXPORT NSString * const API_ModelCoreClassName;
FOUNDATION_EXPORT NSString * const API_ModelCoreUniqueKeys;

#pragma mark - API
FOUNDATION_EXPORT NSString * const API_BaseDevURL;
FOUNDATION_EXPORT NSString * const API_ModelURL;

#pragma mark - HTTP Headers
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeyAppVersion;
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeyAppBuild;
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeyOSName;
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeyOSVersion;
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeySDKVersion;
FOUNDATION_EXPORT NSString * const API_HttpHeaderKeyUserAgent;

#pragma mark - Endpoints
FOUNDATION_EXPORT NSString * const API_EndpointAuthenticate;

#pragma mark - Login Field Keys
FOUNDATION_EXPORT NSString * const API_LoginFieldSessionIDkey;
FOUNDATION_EXPORT NSString * const API_DataKey;

#pragma mark - Test Credentials
FOUNDATION_EXPORT NSString * const USER_Arthur;
FOUNDATION_EXPORT NSString * const USER_Palmer;

@interface Constants : NSObject

@end
