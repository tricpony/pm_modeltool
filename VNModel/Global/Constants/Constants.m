//
//  Constants.m
//  VNModel
//
//  Created by aarthur on 2/10/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import "Constants.h"

@implementation Constants

#pragma mark - API
NSString * const API_BaseDevURL = @"https://mobile.vividnoodle.com/";
NSString * const API_ModelURL = @"https://mobile.vividnoodle.com/mobileapi_model/1";

#pragma mark - Keys
NSString * const API_EmailKey = @"username";
NSString * const API_PasswordKey = @"password";
NSString * const API_VersionKey = @"request_version";
NSString * const API_ModelKey = @"model";
NSString * const API_ModelCoreClassName = @"core";
NSString * const API_ModelCoreUniqueKeys = @"unique_keys";

#pragma mark - HTTP Headers
NSString * const API_HttpHeaderKeyAppVersion = @"PM_APP_VERSION";
NSString * const API_HttpHeaderKeyAppBuild = @"PM_APP_BUILD";
NSString * const API_HttpHeaderKeyOSName = @"PM_OS_NAME";
NSString * const API_HttpHeaderKeyOSVersion = @"PM_OS_VERSION";
NSString * const API_HttpHeaderKeyOSSDKVersion = @"PM_OS_SDK_VERSION";
NSString * const API_HttpHeaderKeyUserAgent = @"User-Agent";

#pragma mark - Endpoints
NSString * const API_EndpointAuthenticate = @"mobileapi_authenticate/1";

#pragma mark - Login Field Keys
NSString * const API_LoginFieldSessionIDkey = @"session_id";
NSString * const API_DataKey = @"payload";

#pragma mark - Test Credentials
NSString * const USER_Arthur = @"aarthur@mobile.vividnoodle.com";
NSString * const USER_Palmer = @"bpalmer@mobile.vividnoodle.com";

@end
