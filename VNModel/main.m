//
//  main.m
//  VNModel
//
//  Created by aarthur on 2/10/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
