//
//  ModelWorkBenchViewController.m
//  VNModel
//
//  Created by aarthur on 2/10/17.
//  Copyright © 2017 PlayMaker. All rights reserved.
//

#import "ModelWorkBenchViewController.h"
#import "Constants.h"

#pragma mark Categories

#import "NSString+Additions.h"
#import "NSColor+Additions.h"

#define FILE_SAVE_DATE_FORMAT_OLD @"MMM_d_yyyy_h_mm_a"
#define FILE_SAVE_DATE_FORMAT @"MMM_d_yyyy_h_mm_ss_a"
#define PULL_DOWN_DATE_FORMAT @"MMM d yyyy h mm ss a"
#define MODEL_FILE_PREFIX @"VN_Model"

@interface ModelWorkBenchViewController ()
@property (weak) IBOutlet NSButton *fetchModelButton;
@property (unsafe_unretained) IBOutlet NSTextView *textView;
@property (strong) NSString *lastPathSaved;
@property (strong) NSString *lastVersionOfModel;
@property (strong) NSArray *lastPulldownTitles;
@property (strong) NSArray *diffRanges;
@property (strong) NSMutableDictionary *uniqueKeys;
@property (assign) NSInteger currentRangeIndex;
@property (strong) NSURL *currentDiffFileURL;
@property (assign) BOOL isFirstAnimation;

@property (weak) IBOutlet NSTextField *uniqueKeyLabel;

@property (weak) IBOutlet NSPopUpButton *diffPullDownButton;
@property (weak) IBOutlet NSPopUpButton *pullDownButton;
@property (weak) IBOutlet NSLayoutConstraint *trailingDiffButtonConstraint;
@property (weak) IBOutlet NSButton *trashCanButton;
@property (weak) IBOutlet NSButton *folderButton;

- (IBAction)fetchModel:(id)sender;
- (IBAction)saveModel:(id)sender;
- (IBAction)jumpToAction:(id)sender;
- (IBAction)advanceToNextDiffAction:(id)sender;
- (IBAction)openDiff:(id)sender;
- (IBAction)showInFinder:(id)sender;
- (IBAction)moveToTrash:(id)sender;

@end

@implementation ModelWorkBenchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fetchModelButton.toolTip = @"Fetch Current Model";
    self.lastPathSaved = [[NSUserDefaults standardUserDefaults] stringForKey:NSStringFromClass([self class])];
    self.lastVersionOfModel = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastVersionOfModel"];
    self.lastPulldownTitles = [[NSUserDefaults standardUserDefaults] arrayForKey:@"lastPulldownTitles"];
    self.uniqueKeys = (id)[[NSUserDefaults standardUserDefaults] dictionaryForKey:API_ModelCoreUniqueKeys];
    
    if (self.lastPulldownTitles) {
        self.pullDownButton.hidden = NO;
        [self.pullDownButton addItemsWithTitles:self.lastPulldownTitles];
        self.textView.string = self.lastVersionOfModel;
    }
    self.currentRangeIndex = 0;
    self.isFirstAnimation = YES;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.view.wantsLayer = YES;
        [self.view makeBackingLayer];
        CALayer *layer;
        
        layer = self.view.layer;
        [layer setBackgroundColor:[NSColor playMakerBlue].CGColor];
    }];
}

- (void)viewWillAppear
{
    [super viewWillAppear];
    self.uniqueKeyLabel.backgroundColor = [NSColor clearColor];
    self.diffPullDownButton.hidden = self.lastVersionOfModel == nil;
    [self fillDiffPullDownChoices];
}

- (void)handleModelDifference
{
    if (![self.lastVersionOfModel length]) {
        [[NSUserDefaults standardUserDefaults] setObject:[self.textView string] forKey:@"lastVersionOfModel"];
        self.lastVersionOfModel = self.textView.string;
    }else{
        __weak __typeof(self)weakSelf = self;

        if (![self.lastVersionOfModel isEqualToString:self.textView.string]) {
            [[NSUserDefaults standardUserDefaults] setObject:[self.textView string] forKey:@"lastVersionOfModel"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                __strong __typeof(weakSelf)strongSelf = weakSelf;

                strongSelf.diffRanges = [strongSelf.lastVersionOfModel rangesOflcsDiff:strongSelf.textView.string];

                dispatch_async(dispatch_get_main_queue(), ^{
                    strongSelf.lastVersionOfModel = strongSelf.textView.string;
                    strongSelf.diffPullDownButton.hidden = strongSelf.diffRanges.count <= 2;

                });
            });
            
        }else{
            self.diffPullDownButton.hidden = YES;
        }
    }

}

- (void)assignHeaderValuesToRequest:(NSMutableURLRequest*)request
{
    [request setValue:@"17.1.1" forHTTPHeaderField:API_HttpHeaderKeyAppVersion];
    [request setValue:@"1" forHTTPHeaderField:API_HttpHeaderKeyAppBuild];
    [request setValue:@"iOS" forHTTPHeaderField:API_HttpHeaderKeyOSName];
    [request setValue:@"10.2.0" forHTTPHeaderField:API_HttpHeaderKeyOSVersion];
    [request setValue:@"iOS" forHTTPHeaderField:API_HttpHeaderKeyUserAgent];
}

- (void)showAlertWithButtonTitle:(NSString*)buttonTitle message:(NSString*)message infoText:(NSString*)infoString
{
    NSAlert *alert = [[NSAlert alloc] init];
    
    [alert addButtonWithTitle:buttonTitle];
    [alert setMessageText:message];
    [alert setInformativeText:infoString];
    [alert setAlertStyle:NSAlertStyleWarning];
    
    [alert runModal];
}

- (void)fillUniqueKeysLabelForJumpToTableName:(NSString*)table
{
    NSString *labelText;
    
    if (self.uniqueKeys) {
        NSRange r;
        
        labelText = [self.uniqueKeys[table] description];
        r = NSMakeRange(0, [labelText length]);
        labelText = [labelText stringByReplacingOccurrencesOfString:@" " withString:@"" options:0 range:r];
        r = NSMakeRange(0, [labelText length]);
        labelText = [labelText stringByReplacingOccurrencesOfString:@"\n" withString:@"" options:0 range:r];
        labelText = [NSString stringWithFormat:@"TABLE: %@  UNIQUED BY: %@",table,labelText];
        self.uniqueKeyLabel.stringValue = labelText;
    }
}

- (IBAction)jumpToAction:(id)sender
{
    NSString *title;
    NSString *regExTitleString;
    NSRange r;
    
    title = [[self.pullDownButton selectedItem] title];
    regExTitleString = [NSString stringWithFormat:@"\\s+%@\\s=",title];
    
    r = [self.textView.string rangeOfString:regExTitleString options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
        r = [self.textView.string rangeOfString:title options:NSLiteralSearch];
    }else{
        
        //scan up to the first character in title and adjust the range
        //the range that matched the reg-ex will select a lot more than
        //what we want so we trim it here
        NSString *matchingString = [self.textView.string substringWithRange:r];
        NSScanner *scanner = [NSScanner scannerWithString:matchingString];
        
        scanner.charactersToBeSkipped = nil;
        [scanner scanUpToString:[title substringToIndex:1] intoString:NULL];
        r.location += [scanner scanLocation];
        r.length = [title length];
    }
    
    if (r.location != NSNotFound) {
        [self.textView scrollRangeToVisible:r];
        [self.textView setSelectedRange:r];
        [self fillUniqueKeysLabelForJumpToTableName:title];
    }
}

- (IBAction)advanceToNextDiffAction:(id)sender
{
    NSRange nextRange;
    
    nextRange = [self.diffRanges[self.currentRangeIndex][1] rangeValue];
    if (nextRange.location != NSNotFound) {
        [self.textView scrollRangeToVisible:nextRange];
        [self.textView setSelectedRange:nextRange];
    }
    
    ++self.currentRangeIndex;
    if (self.currentRangeIndex == [self.diffRanges count]) {
        self.currentRangeIndex = 0;
    }

}

- (void)populateUniqueKeys:(NSDictionary*)coreNode
{
    self.uniqueKeys = [NSMutableDictionary dictionaryWithCapacity:[coreNode count]];
    for (NSString *nextKey in [coreNode allKeys]) {
        self.uniqueKeys[nextKey] = coreNode[nextKey][API_ModelCoreUniqueKeys];
    }
    [[NSUserDefaults standardUserDefaults] setObject:self.uniqueKeys forKey:API_ModelCoreUniqueKeys];
}

#pragma mark Diff

- (void)fillDiffPullDownChoices
{
    if (self.lastVersionOfModel) {
        NSMutableArray *choices;
        
        choices = [NSMutableArray arrayWithCapacity:1];
        
        if (self.lastPathSaved) {
            NSFileManager *fm = [NSFileManager defaultManager];
            NSArray *files;
            NSRange r;
            NSDateFormatter *formatter;

            r = NSMakeRange(0, [MODEL_FILE_PREFIX length]);
            files = [fm contentsOfDirectoryAtPath:self.lastPathSaved error:NULL];
            for (NSString *nextFilename in files) {
                
                //find the model files
                //convert the filenames to dates and collect them in an array
                //sort them descending
                //then convert the dates to strings and collect them in array
                if ([fm fileExistsAtPath:[self.lastPathSaved stringByAppendingPathComponent:nextFilename] isDirectory:NO]) {
                    
                    if (([nextFilename length] > r.length) && [[nextFilename substringWithRange:r] isEqualToString:MODEL_FILE_PREFIX]) {
                        NSString *fileDateString;
                        NSDate *labelDate;
                        
                        formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:FILE_SAVE_DATE_FORMAT];

                        fileDateString = [nextFilename stringByDeletingPathExtension];
                        fileDateString = [fileDateString substringFromIndex:r.length+1];
                        
                        labelDate = [formatter dateFromString:fileDateString];
                        if (labelDate) {
                            [choices addObject:labelDate];
                        }
                    }
                    
                }
            }
            
            //now I have the dates
            //sort them
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:NO];
            [choices sortUsingDescriptors:@[descriptor]];
            
            //now convert them to strings
            NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:[choices count]];
            [formatter setDateFormat:PULL_DOWN_DATE_FORMAT];
            for (NSDate *nextFileDate in choices) {
                NSString *labelString = nil;

                labelString = [formatter stringFromDate:nextFileDate];

                if (labelString) {
                    [tmpArray addObject:labelString];
                }
            }
            choices = tmpArray;
            [choices insertObject:@"Last Version" atIndex:0];

        }
        [self.diffPullDownButton addItemsWithTitles:choices];
    }else{
        self.diffPullDownButton.hidden = YES;
    }
}

- (NSString*)filenameOfPopupChoice
{
    NSString *popupChoice;
    NSString *filename;
    NSDateFormatter *formatter;
    NSDate *labelDate;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:PULL_DOWN_DATE_FORMAT];
    
    popupChoice = [[self.diffPullDownButton selectedItem] title];
    [formatter setDateFormat:PULL_DOWN_DATE_FORMAT];
    labelDate = [formatter dateFromString:popupChoice];
    [formatter setDateFormat:FILE_SAVE_DATE_FORMAT];
    popupChoice = [formatter stringFromDate:labelDate];
    
    filename = [NSString stringWithFormat:@"%@_%@",MODEL_FILE_PREFIX,popupChoice];
    filename = [self.lastPathSaved stringByAppendingPathComponent:filename];
    
    return filename;
}

- (void)openDiff:(id)sender
{
    //first save the models to a temp folder
    if (self.lastVersionOfModel) {
        NSString *oldFilename;
        NSString *newFilename;
        NSURL *tempOldUrl = nil;
        NSURL *tempNewUrl = nil;
        NSError *oldError = nil;
        NSError *newError = nil;
        NSError *error = nil;

        newFilename = [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),@"NewModel"];
        tempNewUrl = [NSURL fileURLWithPath:newFilename isDirectory:NO];
        
        if ([[[self.diffPullDownButton selectedItem] title] isEqualToString:@"Open Diff"]) {
            return;
        }
        
        if ([[[self.diffPullDownButton selectedItem] title] isEqualToString:@"Last Version"]) {
            oldFilename = [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),@"OldModel"];
            tempOldUrl = [NSURL fileURLWithPath:oldFilename isDirectory:NO];
            oldError = [self saveString:self.lastVersionOfModel toURL:tempOldUrl];
        }else{
            oldFilename = [self filenameOfPopupChoice];
        }
        
        newError = [self saveString:self.textView.string toURL:tempNewUrl];
        
        if (!oldError && !newError) {
            NSURL *appURL;
            NSString *appPath;
            
            appPath = [[NSWorkspace sharedWorkspace] fullPathForApplication:@"FileMerge"];
            if (appPath) {
                NSDictionary *launchConfig;
                
                launchConfig = @{NSWorkspaceLaunchConfigurationArguments:@[
                                         @"-left",
                                         [oldFilename stringByAppendingPathExtension:@"txt"],
                                         @"-right",
                                         [newFilename stringByAppendingPathExtension:@"txt"]]
                                         };
                
                
                appURL = [NSURL fileURLWithPath:appPath];
                [[NSWorkspace sharedWorkspace] launchApplicationAtURL:appURL
                                                              options:NSWorkspaceLaunchWithoutAddingToRecents
                                                        configuration:launchConfig
                                                                error:&error];
                
                if (error) {
                    [self showAlertWithButtonTitle:@"OK" message:@"Unable to show model diff." infoText:[error localizedDescription]];
                }else{
                    __weak __typeof(self)weakSelf = self;

                    self.currentDiffFileURL = [NSURL fileURLWithPath:[oldFilename stringByAppendingPathExtension:@"txt"]];
                    [NSAnimationContext runAnimationGroup:^(NSAnimationContext * _Nonnull context) {
                        __strong __typeof(weakSelf)strongSelf = weakSelf;

                        if (strongSelf.isFirstAnimation) {
                            strongSelf.isFirstAnimation = NO;
                            strongSelf.trailingDiffButtonConstraint.constant += 54;
                        }
                    } completionHandler:^{
                        __strong __typeof(weakSelf)strongSelf = weakSelf;

                        strongSelf.trashCanButton.hidden = NO;
                        strongSelf.folderButton.hidden = NO;
                    }];
                }

            }else{
                [self showAlertWithButtonTitle:@"OK" message:@"Unable to show model diff." infoText:@"App FileMerge Not Found."];
            }

        }else{
            if (oldError) {
                [self showAlertWithButtonTitle:@"OK" message:@"Unable to show model diff." infoText:[oldError localizedDescription]];
            }else if (newError) {
                [self showAlertWithButtonTitle:@"OK" message:@"Unable to show model diff." infoText:[newError localizedDescription]];
            }
        }
    }
}

- (IBAction)showInFinder:(id)sender
{
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[self.currentDiffFileURL]];
}

- (IBAction)moveToTrash:(id)sender
{
    NSAlert *alert = [[NSAlert alloc] init];
    __block BOOL confirmFlag;
    
    [alert setMessageText:@"Trash?"];
    [alert addButtonWithTitle:@"Yes"];
    [alert addButtonWithTitle:@"No"];
    [alert setInformativeText:@"Move file to trash?"];

    [alert beginSheetModalForWindow:self.view.window completionHandler:^(NSModalResponse returnCode) {
        if(returnCode == 1000)
        {
            confirmFlag = YES;
        }
        else
        {
            confirmFlag = NO;
        }

        if (confirmFlag) {
            __weak __typeof(self)weakSelf = self;
            
            [[NSWorkspace sharedWorkspace] recycleURLs:@[self.currentDiffFileURL] completionHandler:^(NSDictionary<NSURL *,NSURL *> * newURLs, NSError* error) {
                __strong __typeof(weakSelf)strongSelf = weakSelf;
                NSString *itemTitle;
                
                itemTitle = [[self.currentDiffFileURL absoluteString] lastPathComponent];
                itemTitle = [itemTitle substringFromIndex:[MODEL_FILE_PREFIX length]+1];
                [strongSelf.pullDownButton selectItemAtIndex:0];
                [strongSelf.pullDownButton removeItemWithTitle:itemTitle];
            }];
        }
    }];
}

#pragma mark Save

- (NSError*)saveString:(NSString*)modelString toURL:(NSURL*)url
{
    NSError *error = nil;
    
    [modelString writeToURL:[url URLByAppendingPathExtension:@"txt"] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    return error;
}

- (IBAction)saveModel:(id)sender
{
    NSString *savePath = NSHomeDirectory();
    NSSavePanel *savePanel;
    NSDateFormatter *formatter;
    NSString *defaultFileName;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:FILE_SAVE_DATE_FORMAT];
    defaultFileName = [NSString stringWithFormat:@"%@_%@",MODEL_FILE_PREFIX,[formatter stringFromDate:[NSDate date]]];
    
    savePanel = [NSSavePanel savePanel];
    savePanel.nameFieldStringValue = defaultFileName;
    if (self.lastPathSaved) {
        savePath = self.lastPathSaved;
    }
    savePanel.directoryURL = [NSURL fileURLWithPath:savePath];
    
    [savePanel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            NSError *error = nil;
            
            [[NSUserDefaults standardUserDefaults] setObject:[savePanel.directoryURL path] forKey:NSStringFromClass([self class])];
            error = [self saveString:self.textView.string toURL:savePanel.URL];
            if (error) {
                [self showAlertWithButtonTitle:@"OK" message:@"Save Failed" infoText:[error localizedDescription]];
            }else{
                [self fillDiffPullDownChoices];
            }
        }
    }];
    
}

#pragma mark Network

- (IBAction)fetchModel:(id)sender
{
    __weak __typeof(self)weakSelf = self;
    
    [self.textView setString:@""];
    [self fetchSessionWithCompletionBlock:^(NSString *sessionID, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        
        if (sessionID) {
            NSMutableURLRequest *request;
            NSURLSession *urlSession;
            NSString *sessionURLString;
            NSURL *sessionURL;
            
            sessionURLString = API_ModelURL;
            sessionURL = [NSURL URLWithString:sessionURLString];
            request = [NSMutableURLRequest requestWithURL:sessionURL];
            [request setValue:sessionID forHTTPHeaderField:@"PM_SESSION_ID"];
            request.HTTPMethod = @"PUT";
            
            urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSDictionary *responseInfo = [NSJSONSerialization JSONObjectWithData:data
                                                                             options:0
                                                                               error:nil];
                
                if (error) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [strongSelf showAlertWithButtonTitle:@"OK" message:@"Model Fetch Failed" infoText:[error localizedDescription]];
                    }];
                }else{
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        NSArray *pullDownTitles;
                        
                        [self populateUniqueKeys:responseInfo[API_DataKey][API_ModelKey][API_ModelCoreClassName]];
                        pullDownTitles = [responseInfo[API_DataKey][API_ModelKey][API_ModelCoreClassName] allKeys];
                        strongSelf.pullDownButton.hidden = [pullDownTitles count];
                        pullDownTitles = [pullDownTitles sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                        [[NSUserDefaults standardUserDefaults] setObject:pullDownTitles forKey:@"lastPulldownTitles"];
                        [strongSelf.pullDownButton addItemsWithTitles:pullDownTitles];
                        [strongSelf.textView setString:[responseInfo description]];
                        self.uniqueKeyLabel.stringValue = @"";
                        [strongSelf handleModelDifference];
                    }];

                }
                
            }] resume];
            [urlSession finishTasksAndInvalidate];

        }else{
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (error) {
                    strongSelf.textView.string = strongSelf.lastVersionOfModel;
                    [strongSelf showAlertWithButtonTitle:@"OK" message:@"Session Fetch Failed" infoText:[error localizedDescription]];
                }else{
                    [strongSelf.textView setString:[error localizedDescription]];
                }
            }];

        }
        
    }];
    
}

- (void)fetchSessionWithCompletionBlock:(void(^)(NSString* sessionID, NSError *error))block
{
    NSMutableURLRequest *request;
    NSURLSession *urlSession;
    NSString *sessionURLString;
    NSURL *sessionURL;
    NSURLSessionConfiguration *config;
    NSURLSessionTask *sessionTask;
    NSDictionary *params;
    NSData *httpBody;
    
    params = @{
               API_EmailKey:USER_Palmer,
               API_PasswordKey:@"jjH7yqp59Peg2m48",
               API_VersionKey:@(1)
               };
    
    httpBody = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 5;
    
    sessionURLString = [NSString stringWithFormat:@"%@%@",API_BaseDevURL,API_EndpointAuthenticate];
    sessionURL = [NSURL URLWithString:sessionURLString];
    request = [NSMutableURLRequest requestWithURL:sessionURL];
    [self assignHeaderValuesToRequest:request];
    request.HTTPMethod = @"PUT";
    request.HTTPBody = httpBody;
    
    urlSession = [NSURLSession sessionWithConfiguration:config];
    sessionTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *sesionValue = nil;
        
        if ([(NSHTTPURLResponse*)response statusCode] == 404) {
            NSError *missingContentError;
            NSDictionary *errorDictionary = @{ NSLocalizedDescriptionKey : @"Got 404 Error" };
            
            missingContentError = [[NSError alloc] initWithDomain:@"??" code:1 userInfo:errorDictionary];
            error = missingContentError;
            
        }else if (data && !error) {  //it worked
            NSDictionary *responseInfo = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
            
            sesionValue = responseInfo[API_DataKey][API_LoginFieldSessionIDkey];
            
        }
        
        block(sesionValue,error);
    }];
    [sessionTask resume];
    [urlSession finishTasksAndInvalidate];
}

@end
